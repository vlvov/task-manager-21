package ru.t1.vlvov.tm.repository;

import ru.t1.vlvov.tm.api.repository.ITaskRepository;
import ru.t1.vlvov.tm.model.Task;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public final class TaskRepository extends AbstractUserOwnedRepository<Task> implements ITaskRepository {

    @Override
    public List<Task> findAllByProjectId(final String projectId) {
        return models.stream()
                .filter(m -> projectId.equals(m.getProjectId()))
                .collect(Collectors.toList());
    }

    @Override
    public List<Task> findAllByProjectId(final String userId, final String projectId) {
        return models.stream()
                .filter(m -> userId.equals(m.getUserId()))
                .filter(m -> projectId.equals(m.getProjectId()))
                .collect(Collectors.toList());
    }

}
