package ru.t1.vlvov.tm.command.task;

import ru.t1.vlvov.tm.util.TerminalUtil;

public final class TaskBindToProjectCommand extends AbstractTaskCommand {

    private final String DESCRIPTION = "Bind task to Project.";

    private final String NAME = "task-bind-to-project";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[BIND TASK TO PROJECT]");
        System.out.println("ENTER PROJECT ID:");
        final String projectId = TerminalUtil.nextLine();
        System.out.println("ENTER TASK ID:");
        final String taskId = TerminalUtil.nextLine();
        final String userId = getAuthService().getUserId();
        getProjectTaskService().bindTaskToProject(userId, projectId, taskId);
    }

}