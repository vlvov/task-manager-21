package ru.t1.vlvov.tm.command.task;

import ru.t1.vlvov.tm.enumerated.Status;
import ru.t1.vlvov.tm.util.TerminalUtil;

import java.util.Arrays;

public final class TaskChangeStatusByIdCommand extends AbstractTaskCommand {

    private final String DESCRIPTION = "Change task status by Id.";

    private final String NAME = "task-change-status-by-id";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[CHANGE TASK STATUS BY ID]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        System.out.println("ENTER STATUS:");
        System.out.println(Arrays.toString(Status.values()));
        final String statusValue = TerminalUtil.nextLine();
        final Status status = Status.toStatus(statusValue);
        final String userId = getAuthService().getUserId();
        getTaskService().changeTaskStatusById(userId, id, status);
    }

}