package ru.t1.vlvov.tm.command.system;

import ru.t1.vlvov.tm.api.model.ICommand;
import ru.t1.vlvov.tm.command.AbstractCommand;

import java.util.Collection;

public final class ApplicationHelpCommand extends AbstractSystemCommand {

    private final String ARGUMENT = "-h";

    private final String DESCRIPTION = "Show application commands.";

    private final String NAME = "help";

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[HELP]");
        final Collection<AbstractCommand> commands = getCommandService().getTerminalCommands();
        for (final ICommand command : commands) System.out.println(command);
    }

}
