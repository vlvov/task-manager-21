package ru.t1.vlvov.tm.command.task;

import ru.t1.vlvov.tm.model.Task;
import ru.t1.vlvov.tm.util.TerminalUtil;

public final class TaskShowByIndexCommand extends AbstractTaskCommand {

    private final String DESCRIPTION = "Show task by Index.";

    private final String NAME = "task-show-by-index";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[SHOW TASK BY INDEX]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final String userId = getAuthService().getUserId();
        final Task task = getTaskService().findOneByIndex(userId, index);
        showTask(task);
    }

}