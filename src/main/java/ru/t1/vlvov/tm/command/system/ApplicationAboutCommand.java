package ru.t1.vlvov.tm.command.system;

public final class ApplicationAboutCommand extends AbstractSystemCommand {

    private final String ARGUMENT = "-a";

    private final String DESCRIPTION = "Show developer info.";

    private final String NAME = "about";

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[ABOUT]");
        System.out.println("Name: Vladimir Lvov");
        System.out.println("E-mail: vlvov@t1.ru");
    }

}
