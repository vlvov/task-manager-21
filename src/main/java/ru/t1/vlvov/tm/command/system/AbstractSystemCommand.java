package ru.t1.vlvov.tm.command.system;

import ru.t1.vlvov.tm.api.service.ICommandService;
import ru.t1.vlvov.tm.api.service.ILoggerService;
import ru.t1.vlvov.tm.command.AbstractCommand;
import ru.t1.vlvov.tm.enumerated.Role;

public abstract class AbstractSystemCommand extends AbstractCommand {

    protected ICommandService getCommandService() {
        return serviceLocator.getCommandService();
    }

    protected ILoggerService getLoggerService() {
        return serviceLocator.getLoggerService();
    }

    @Override
    public Role[] getRoles() {
        return null;
    }

}
