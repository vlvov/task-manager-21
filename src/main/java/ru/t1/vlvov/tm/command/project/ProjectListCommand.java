package ru.t1.vlvov.tm.command.project;

import ru.t1.vlvov.tm.enumerated.Sort;
import ru.t1.vlvov.tm.model.Project;
import ru.t1.vlvov.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public final class ProjectListCommand extends AbstractProjectCommand {

    private final String DESCRIPTION = "Display all projects.";

    private final String NAME = "project-list";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[PROJECT LIST]");
        System.out.println("SELECT SORT:");
        System.out.println(Arrays.toString(Sort.values()));
        final String sortName = TerminalUtil.nextLine();
        final Sort sort = Sort.toSort(sortName);
        final String userId = getAuthService().getUserId();
        final List<Project> projects = getProjectService().findAll(userId, sort);
        int index = 1;
        for (final Project project : projects) {
            showProject(project);
            //if (project == null) continue;
           // System.out.println(index + ". " + project);
           // index++;
        }
    }

}