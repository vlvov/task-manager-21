package ru.t1.vlvov.tm.command.project;

import ru.t1.vlvov.tm.model.Project;
import ru.t1.vlvov.tm.util.TerminalUtil;

public final class ProjectShowByIdCommand extends AbstractProjectCommand {

    private final String DESCRIPTION = "Show project by Id.";

    private final String NAME = "project-show-by-id";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[SHOW PROJECT BY ID]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final String userId = getAuthService().getUserId();
        final Project project = getProjectService().findOneById(userId, id);
        showProject(project);
    }

}