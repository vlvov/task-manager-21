package ru.t1.vlvov.tm.exception.field;

import ru.t1.vlvov.tm.exception.AbstractException;

public abstract class AbstractFieldNotFoundException extends AbstractException {

    public AbstractFieldNotFoundException() {
        super();
    }

    public AbstractFieldNotFoundException(final String message) {
        super(message);
    }

    public AbstractFieldNotFoundException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public AbstractFieldNotFoundException(final Throwable cause) {
        super(cause);
    }

    protected AbstractFieldNotFoundException(final String message, final Throwable cause, final boolean enableSuppression, final boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
